#include <spring\Framework\Application.h>
#include <spring\Application\ApplicationModel.h>
#include<QApplication.h>

int main(int argc, char **argv)
{
	QApplication qAplication(argc, argv);

	Spring::Application& application = Spring::Application::getInstance();

	Spring::ApplicationModel applicationModel;

	application.setApplicationModel(&applicationModel);

	application.start("Level 1",420, 420);

	qAplication.exec();

	return 0;
}