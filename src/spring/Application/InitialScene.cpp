#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		if (m_uMainWindow->objectName().isEmpty())
			m_uMainWindow->setObjectName(QStringLiteral("MainWindow"));
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));
		label->setAlignment(Qt::AlignCenter);

		gridLayout->addWidget(label, 0, 0, 1, 1);

		m_uMainWindow->setCentralWidget(centralWidget);
		label->setText(QApplication::translate("MainWindow", "Hello World", Q_NULLPTR));
	}

}

